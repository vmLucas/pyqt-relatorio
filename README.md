## Projeto de software para processamento de dados de espectroscopia Raman

É possível rodar o código em um ambiente de Python 3.10.4 com os seguintes requisitos:
- PySide6
- pyqtgraph
- numpy
- pandas
- peakutils
- scipy

Também foi adicionado um executável para facilitar os testes do softwaere. É só baixar e executar o arquivo dist/main.exe ou baixar o arquivo presente na aba releases. 
