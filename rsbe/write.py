from os.path import exists
import logging
import os
import sys

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)


def write_file(write_content, filepath):
    '''Write_content is a tuple with 4 items: Title, geometry, boolean to know if data is plotted and data'''
    with open(filepath + '.rsbe', 'w', encoding='utf-8') as file:
        file.write('[Raman software basic extension]\nMade by Lucas Militão @ LaMuCrEs, IFSC, USP.'
                   ' lucas.milito@usp.br\n')
        for i in range(len(write_content)):
            title, geometry, boolean_p, data = write_content[i]
            file.write(f'!-{title}-!\n')
            file.write(f'!-plot={boolean_p}-!\n')
            file.write(f'!-geometry={geometry}-!\n')
            file.writelines([f'{y[0]}, {y[1]}\n' for y in data])
            file.writelines('!--!\n')
