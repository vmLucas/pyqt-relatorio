from os.path import exists
import logging
import os
import sys

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)


def open_file(filepath):
    sp_dics = {}
    b = []
    e = []
    if not exists(filepath):
        raise Exception('Filepath does not exist')
    with open(filepath, 'r', encoding='utf-8') as file:
        data = file.readlines()
        if data[0].strip() != '[Raman software basic extension]':
            raise Exception('Wrong filetype, seems to be corrupted.')
        else:
            for i in range(len(data)):
                data[i] = data[i].strip()
                # End of file
                if data[i] == '!----!':
                    break
                # Comments
                elif data[i][:2] == '//':
                    continue
                # plot
                elif data[i][:6] == '!-plot':
                    continue
                # skip geometry
                elif data[i][:10] == '!-geometry':
                    logger.info(data[i])
                elif data[i][:2] == '!-':
                    if data[i] != '!--!':
                        b.append(i)
                        sp_name = data[i][2:-2]
                        logger.info('Spreadsheet name: ' + sp_name)
                    else:
                        e.append(i)
                        logger.info('Spreadsheet end.')
                        sp_dics[sp_name] = data[b[-1]+1:e[-1]]
            logger.info(sp_dics)
    return sp_dics