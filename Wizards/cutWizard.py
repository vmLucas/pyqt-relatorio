from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtWidgets import QDialogButtonBox, QGridLayout, QListWidget, QPushButton, QLabel, \
    QLineEdit, QWidget, QMessageBox, QCheckBox, QMdiSubWindow
from PySide6 import QtUiTools
from PySide6.QtUiTools import QUiLoader
from PySide6.QtCore import QFile
import pyqtgraph as pg
from pyqtgraph import PlotWidget
from peakutils import baseline
from Wizards.commumUsed import new_spreadsheet
from Spreadsheet import NewSpreadWindow
import numpy as np


class CutWizard(QWidget):
    def __init__(self, data, sindex, mdi, cursub):
        super().__init__()
        loader = QUiLoader()
        loader.registerCustomWidget(PlotWidget)
        self.data = data
        self.ui = CutWizard.loadUiWidget(self, r'C:\Users\Lucas\Documents\ic\pyqt-relatorio\Wizards\cutWizard.ui')
        layout = QGridLayout()
        layout.addWidget(self.ui, 0, 0)
        self.setLayout(layout)
        # Preview plot
        self.dialogButtons = self.findChild(QDialogButtonBox, 'buttonBox')
        self.dialogButtons.accepted.connect(self.accept)
        self.dialogButtons.rejected.connect(self.reject)
        self.glWidget = self.findChild(PlotWidget, 'plotWidget')
        self.parentWidth = super().geometry().width()
        self.parentHeight = super().geometry().height()
        self.setGeometry(0, 0, self.parentWidth, self.parentHeight)

        self.mdi2 = cursub
        if self.mdi2 == None:
            self.mdi = mdi
        else:
            self.mdi = list(sindex.keys())[list(sindex.values()).index(cursub)].parent().parent()

        self.previewPlot()

    def loadUiWidget(self, uifilename):
        loader = QtUiTools.QUiLoader()
        file = QFile(uifilename)
        file.open(QFile.ReadOnly)
        ui = loader.load(file, self)
        file.close()
        return ui

    def find_nearest(self, array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return idx

    def previewPlot(self):
        plot = self.glWidget
        plot.plot(self.data)

        self.lr = pg.LinearRegionItem([self.data[0][0], self.data[len(self.data)//2][0]],
                                 bounds=[self.data[0][0], self.data[len(self.data) - 1][0]], movable=True)
        plot.addItem(self.lr)

    def new_spreadsheet(self, dataFinal):
        return new_spreadsheet(self.mdi, dataFinal)

    def accept(self):
        values = self.lr.getRegion()
        self.dataT = self.data.T
        data_spread = self.data[self.find_nearest(self.dataT, values[0]):self.find_nearest(
            self.dataT, values[1])][:]
        sp1 = self.new_spreadsheet(data_spread)
        self.mdi.addSubWindow(sp1)
        sp1.show()
        self.close()

    def reject(self):
        self.close()

