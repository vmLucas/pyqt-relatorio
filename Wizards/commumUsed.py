from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtWidgets import QDialogButtonBox, QGridLayout, QListWidget, QPushButton, QLabel, \
    QLineEdit, QWidget, QMessageBox, QCheckBox, QMdiSubWindow
from Spreadsheet import NewSpreadWindow

def new_spreadsheet(mdi, dataFinal):
    sp = NewSpreadWindow()
    tableWidget = sp.tableWidget

    # Create Sub Windows
    sub = QMdiSubWindow()
    sub.setWindowTitle(str(len(mdi.subWindowList())))
    sub.setWidget(sp)
    sub.setMaximumWidth(250)

    # Add the sub window into our mdi widget
    print(len(dataFinal[0]))
    tableWidget.setRowCount(len(dataFinal))
    tableWidget.setColumnCount(len(dataFinal[0]))
    for i in range(len(dataFinal)):
        for j in range(len(dataFinal[i])):
            tableWidget.setItem(i, j, QtWidgets.QTableWidgetItem(str(dataFinal[i, j])))
    return sub