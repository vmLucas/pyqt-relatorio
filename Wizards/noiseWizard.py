from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtWidgets import QDialogButtonBox, QGridLayout, QListWidget, QPushButton, QLabel, \
    QLineEdit, QWidget, QMessageBox, QCheckBox, QMdiSubWindow, QComboBox, QSpinBox
from PySide6 import QtUiTools
from PySide6.QtUiTools import QUiLoader
from PySide6.QtCore import QFile
import pyqtgraph as pg
from peakutils import baseline
from Spreadsheet import NewSpreadWindow
from Wizards.commumUsed import new_spreadsheet
import numpy as np
from scipy.signal import savgol_filter as sgf


class NoiseWizard(QtWidgets.QWidget):
    def __init__(self, data, sindex, cursub):
        super().__init__()
        loader = QUiLoader()
        self.ui = NoiseWizard.loadUiWidget(self, r'C:\Users\Lucas\Documents\ic\pyqt-relatorio\Wizards\noiseWizard.ui')
        layout = QGridLayout()
        layout.addWidget(self.ui, 0, 0)
        self.setLayout(layout)
        self.setWindowTitle('Noise reduction wizard')
        self.data = data
        # preview button
        self.previewButton = self.findChild(QPushButton, 'previewButton')
        self.previewButton.clicked.connect(self.preview)

        # Algorithm selection
        self.list = self.findChild(QListWidget, 'listWidget')
        self.listAdd('Savitzky-Golay')
        self.list.setCurrentItem(self.list.itemAt(-1,0))

        # configs
        self.modeBox = self.findChild(QComboBox, 'modeBox')
        self.windowLength = self.findChild(QSpinBox, 'windowLengthSpin')
        self.polynomialOrder = self.findChild(QSpinBox, 'polynomialOrderSpin')
        self.deltaLEdit = self.findChild(QLineEdit, 'deltaLineEdit')

        #apply button
        self.applyb = self.findChild(QPushButton, 'pushButton')
        self.applyb.clicked.connect(self.applyButton)

        # mode box add modes
        self.modeBox.addItems(['interpolate', '‘wrap’', 'nearest', 'constant', 'mirror'])

        # default values
        self.modeBox.setCurrentIndex(0)
        self.windowLength.setValue(1)
        self.polynomialOrder.setValue(0)
        self.deltaLEdit.setText(str(1.0))

        # handling mdiareas
        self.mdi = list(sindex.keys())[list(sindex.values()).index(cursub)].parent().parent()
        self.mdi2 = cursub

        # close/ok buttons
        self.buttonBox = self.findChild(QDialogButtonBox, 'buttonBox')
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def loadUiWidget(self, uifilename):
        loader = QtUiTools.QUiLoader()
        file = QFile(uifilename)
        print(uifilename)
        file.open(QFile.ReadOnly)
        ui = loader.load(file, self)
        file.close()
        return ui

    def applySavgol(self):
        xdata = self.data.T[0]
        ydata = self.data.T[1]
        if self.modeBox.currentText() == 'Interpolate':
            self.windowLength.setMaximum(len(self.data))
        win_length = self.windowLength.value()
        polyorder = self.polynomialOrder.value()
        try:
            delta = float(self.deltaLEdit.text())
        except:
            print('not a number')
            delta = 0
        civ = self.modeBox.currentIndex()
        if civ == 0:
            c_text = 'interp'
        elif civ == 1:
            c_text = 'wrap'
        elif civ == 2:
            c_text = 'nearest'
        elif civ == 3:
            c_text = 'constant'
        else:
            c_text = 'mirror'
        savgol = sgf(ydata, win_length, polyorder, 0, delta, mode=c_text)
        self.newData = np.array([xdata, savgol]).T

    def preview(self):
        self.plotWidget = QWidget
        self.applySavgol()
        self.gw = pg.PlotWidget()
        self.gw.plot(self.newData)
        self.gw.show()
        self.gw.setGeometry(self.geometry().x() + self.geometry().width(), self.geometry().y(), self.gw.geometry().width(),
                       self.geometry().height())
        self.plotWidget.show()

    def listAdd(self, itemName):
        '''Add scripts to the list'''
        self.list.addItem(itemName)
        self.list.setCurrentItem(self.list.itemAt(-1,0))

    def applyButton(self):
        self.gw.clear()
        self.applySavgol()
        self.gw.plot(self.newData)
        self.gw.show()

    def accept(self):
        self.applySavgol()
        self.mdi.addSubWindow(new_spreadsheet(self.mdi, self.newData))
        self.gw.close()
        self.close()

    def reject(self):
        self.gw.close()
        self.close()



