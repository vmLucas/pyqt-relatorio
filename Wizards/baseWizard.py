from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtWidgets import QDialogButtonBox, QGridLayout, QListWidget, QPushButton, QLabel, \
    QLineEdit, QWidget, QMessageBox, QCheckBox, QMdiSubWindow
from PySide6 import QtUiTools
from PySide6.QtUiTools import QUiLoader
from PySide6.QtCore import QFile
import pyqtgraph as pg
from peakutils import baseline
from Spreadsheet import NewSpreadWindow
from Wizards.commumUsed import new_spreadsheet
import numpy as np


class BaselineWizard(QtWidgets.QWidget):
    def __init__(self, currentData, sindex, cursub):
        super().__init__()
        # baseline wizard ui
        loader = QUiLoader()
        self.ui = BaselineWizard.loadUiWidget(self,
                                              r'C:\Users\Lucas\Documents\ic\pyqt-relatorio\Wizards\baselinewizard.ui')
        layout = QGridLayout()
        layout.addWidget(self.ui, 0, 0)
        self.buttonBox = self.findChild(QDialogButtonBox, 'buttonBox')
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.data = currentData

        self.list = self.findChild(QListWidget, 'listWidget')
        # Preview baseline
        self.listPreview = self.findChild(QPushButton, 'previewButton')
        self.listPreview.clicked.connect(self.previewBaseline)

        # preview subtraction
        self.previewSubtraction = self.findChild(QPushButton, 'previewSubtractionButton')
        self.previewSubtraction.clicked.connect(self.previewBaselineSubtraction)
        # iterations
        self.textInput = self.findChild(QLineEdit, 'iterationsInput')
        self.applyItButton = self.findChild(QPushButton, 'applyIterationsButton')
        self.applyItButton.clicked.connect(self.applyButtonFunc)

        # degree
        self.degreeInput = self.findChild(QLineEdit, 'degreeEdit')
        self.applyDegree = self.findChild(QPushButton, 'applyDegree')
        self.applyDegree.clicked.connect(self.applyDegreeFunc)

        # tolerance
        self.toleranceInput = self.findChild(QLineEdit, 'toleranceEdit')
        self.applyTolerance = self.findChild(QPushButton, 'applyTolerance')
        self.applyTolerance.clicked.connect(self.applyToleranceFunc)

        # Checkboxes
        self.lesserThanZeroCheck = self.findChild(QCheckBox, 'lesserTZero')
        self.subtractBaselineCheck = self.findChild(QCheckBox, 'subtractBaseline')
        self.createNewDatasetCheck = self.findChild(QCheckBox, 'newDataset')

        # Add algorithms to the list
        self.listAdd('baselineRemoval')
        # xData, Ydata
        self.xData = np.array([i[0] for i in self.data])
        self.yData = self.yData = np.array([i[1] for i in self.data])

        # handling mdiareas
        self.mdi = list(sindex.keys())[list(sindex.values()).index(cursub)].parent().parent()
        self.mdi2 = cursub
        self.setLayout(layout)

    def loadUiWidget(self, uifilename):
        loader = QtUiTools.QUiLoader()
        file = QFile(uifilename)
        print(uifilename)
        file.open(QFile.ReadOnly)
        ui = loader.load(file, self)
        file.close()
        return ui

    def applyButtonFunc(self):
        self.previewBaseline()

    def applyDegreeFunc(self):
        self.previewBaseline()

    def applyToleranceFunc(self):
        self.previewBaseline()

    def listAdd(self, itemName):
        '''Add scripts to the list'''
        self.list.addItem(itemName)
        self.list.setCurrentItem(self.list.itemAt(-1,0))

    def previewBaseline(self):
        self.plotBaseline()

    def previewBaselineSubtraction(self):
        self.plotBaseline(True)

    def baselineCalc(self):
        if self.textInput.text().strip() == '':
            iterations = 300
        else:
            try:
                iterations = int(self.textInput.text())
            except:
                erm = self.error('Please insert a number in the iterations slot.')
                erm.show()
                erm.activateWindow()
                self.textInput.setText('')

        if self.degreeInput.text().strip() == '':
            deg = 3
        else:
            try:
                deg = int(self.degreeInput.text())
            except:
                erm2 = self.error('Please insert a number in the degree slot.')
                erm2.show()
                erm2.activateWindow()
                self.degreeInput.setText('')

        if self.toleranceInput.text().strip() == '':
            tolerance = 1e-3
        else:
            try:
                tolerance = float(self.toleranceInput.text())
            except:
                erm3 = self.error('Please insert a number in the tolerance slot.')
                erm3.show()
                erm3.activateWindow()
                self.toleranceInput.setText('')
        return baseline(self.yData, deg, iterations, tolerance)

    def plotBaseline(self, subtract=False):
        calcBaseline = self.baselineCalc()
        self.plotWidget = QWidget
        gw = pg.PlotWidget()
        if subtract:
            if self.lesserThanZeroCheck.isChecked():
                subtracted_baseline = self.yData - calcBaseline
                for i in range(len(subtracted_baseline)):
                    if subtracted_baseline[i] < 0:
                        subtracted_baseline[i] = 0
                gw.plot(self.xData, subtracted_baseline)
            else:
                gw.plot(self.xData, self.yData - calcBaseline)
        else:
            gw.plot(self.data)
            gw.plot(self.xData, calcBaseline)
        gw.show()
        gw.setGeometry(self.geometry().x()+self.geometry().width(), self.geometry().y(), gw.geometry().width(),
                       self.geometry().height())
        # print(self.pos(), self.geometry().x())
        self.plotWidget.show()

    def new_spreadsheet(self, dataFinal):
        return new_spreadsheet(self.mdi, dataFinal)

    def error(self, errorMessage):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setText("Error")
        msg.setInformativeText(errorMessage)
        msg.setWindowTitle("Error")
        return msg

    def accept(self):
        # obtaining sub window parent
        newY = self.yData - self.baselineCalc()
        if self.lesserThanZeroCheck.isChecked():
            for i in range(len(newY)):
                if newY[i] < 0:
                    newY[i] = 0
        newData = np.array([self.xData, newY])
        self.mdi.addSubWindow(self.new_spreadsheet(newData.T))
        print('[log] ', len(self.xData), newY, newData.T)
        self.close()
        self.plotWidget.close()

    def reject(self):
        self.close()
        self.plotWidget.close()

