import pandas as pd
from PySide6.QtWidgets import QTabWidget, QMainWindow, QApplication, QStatusBar, QMenu, QMenuBar, QLabel, QPushButton, QMdiArea, QMdiSubWindow, QTextEdit
from PySide6.QtUiTools import QUiLoader
from PySide6.QtGui import QAction
from PySide6 import QtUiTools
from PySide6.QtCore import QEvent, Qt
import pyqtgraph as pg

import rsbe.write
from Spreadsheet import *
from mdiSubWindowCustom import MdiSubWindow
import sys
import signal
from Wizards.baseWizard import BaselineWizard
from Wizards.noiseWizard import NoiseWizard
from Wizards.cutWizard import CutWizard
QSS = """

"""


class UI(QMainWindow):
    def __init__(self, ui):
        super(UI, self).__init__()
        self.initUI(ui)

    def initUI(self, ui):
        loader = QUiLoader()
        loader.registerCustomWidget(MdiFixBoundaries)
        self.ui = UI.loadUiWidget(self, ui)
        self.mdi = self.findChild(MdiFixBoundaries, "mdiArea")
        self.mdi2 = self.findChild(MdiFixBoundaries, "mdiArea_2")
        self.menuBar = self.findChild(QMenuBar, 'menubar')
        self.menuFile = self.findChild(QMenu, 'menuFile')
        self.menuEdit = self.findChild(QMenu, 'menuEdit')
        self.statusBar = self.findChild(QStatusBar, 'statusbar')
        self.mdi.installEventFilter(self)

        # spreadsheet menu entry
        self.newSpreadsheet = self.findChild(QtGui.QAction, 'actionNew_Spreadsheet')
        self.newSpreadsheet.triggered.connect(self.new_spreadsheet)

        # open menu entry
        self.open = self.findChild(QtGui.QAction, 'actionOpen')
        self.open.setStatusTip('Open File')
        self.open.triggered.connect(self.file_open)



        # save menu entry
        self.save = self.findChild(QtGui.QAction, 'actionSave')
        self.save.setStatusTip('Save a project to a file')
        self.save.triggered.connect(self.save_project)

        self.tabwidget = self.findChild(QTabWidget, 'tabWidget')
        self.tab2 = self.findChild(QtWidgets.QWidget, 'tab_2')
        self.mdi2.installEventFilter(self)

        self._titleColor = QtGui.QColor()

        self.dataPlott = {}
        self.ui.installEventFilter(self)
        self.ui.show()

    def loadUiWidget(self, uifilename):
        loader = QtUiTools.QUiLoader()
        uifile = QtCore.QFile(uifilename)
        uifile.open(QtCore.QFile.ReadOnly)
        ui = loader.load(uifile, self)
        uifile.close()
        return ui

    def eventFilter(self, source, event):
        if event.type() == QEvent.Close and source == self.ui:
            sys.exit(0)
        elif event.type() == QEvent.ContextMenu:
            if source is self.mdi:
                menu = QMenu()
                # Save action
                ac_save = QAction()
                ac_save.setText('Save')
                menu.addAction(ac_save)
                # plot action
                ac_plot = QAction()
                ac_plot.setText('Plot')
                menu.addAction(ac_plot)
                ac_plot.setStatusTip('why')
                # cut data action
                ac_cut = QAction()
                ac_cut.setText('Cut')
                menu.addAction(ac_cut)
                menu_action = menu.exec(event.globalPos())
                if menu_action == ac_plot:
                    # Activates when plot is selected from the context menu
                    self.plot()
                    self.tabwidget.setCurrentWidget(self.tab2)
                    return True
                elif menu_action == ac_cut:
                    self.cutWizard(self.get_data(self.mdi.currentSubWindow().widget().tableWidget))

                elif menu_action == ac_save:
                    self.file_save()
                return True
            elif source is self.mdi2:
                menu = QMenu()
                # baseline
                acBaseline = QAction()
                acBaseline.setText('baseline')
                menu.addAction(acBaseline)

                # noise
                acNoise = QAction()
                acNoise.setText('Noise reduction')
                menu.addAction(acNoise)
                # cut data action
                ac_cut = QAction()
                ac_cut.setText('cut')
                menu.addAction(ac_cut)
                menu_action2 = menu.exec(event.globalPos())
                if menu_action2 == acBaseline:
                    # Activates when plot is selected from the context menu
                    print(self.mdi2.activeSubWindow())
                    self.baselineWizard(self.dataPlott, self.mdi2.currentSubWindow())
                    return True
                elif menu_action2 == acNoise:
                    # Activates when noise is selected from the context menu
                    self.noiseWizard(self.dataPlott, self.mdi2.currentSubWindow())
                    return True
                elif menu_action2 == ac_cut:
                    ac_cut.setStatusTip('Cut the information to see a specific part of the plot.')
                    self.cutWizard(self.get_data(list(self.dataPlott.keys())[list(self.dataPlott.values())
                                                  .index(self.mdi2.currentSubWindow())].widget().tableWidget))
                return True
        return False

    def cutWizard(self, data):
        self.cutw = CutWizard(data, self.dataPlott, self.mdi, self.mdi2.currentSubWindow())
        self.cutw.show()

    def baselineWizard(self, sindex, cursub):
        self.basew = BaselineWizard(self.get_data(list(sindex.keys())[list(sindex.values()).index(cursub)]
                                                  .widget().tableWidget), sindex, cursub)
        self.basew.show()

    def noiseWizard(self, sindex, cursub):
        self.noisew = NoiseWizard(self.get_data(list(sindex.keys())[list(sindex.values()).index(cursub)]
                                                  .widget().tableWidget), sindex, cursub)
        self.noisew.show()

    def plot(self):
        data = self.get_data(self.mdi.currentSubWindow().widget().tableWidget)
        gw = pg.PlotWidget()
        plotwidget = QMdiSubWindow()
        plotwidget.setWidget(gw)
        self.mdi2.addSubWindow(plotwidget)
        self.dataPlott[self.mdi.currentSubWindow()] = plotwidget
        gw.plot(data)
        gw.show()

    def get_data(self, table):
        data = []
        for i in range(table.rowCount()):
            column = []
            for j in range(table.columnCount()):
                column.append(float(table.item(i, j).text()))
            data.append(column)
        return np.array(data)

    def save_project(self):
        filepath = QtWidgets.QFileDialog.getSaveFileName(self, 'Save Project', filter='*.rsbe')[0]
        mdi1Slist = self.mdi.subWindowList()
        datalist = []
        for i in range(len(mdi1Slist)):
            print(self.dataPlott[mdi1Slist[i]])
            datalist.append((mdi1Slist[i].windowTitle(), mdi1Slist[i].geometry(), self.dataPlott[mdi1Slist[i]] != (),
                             self.get_data(mdi1Slist[i].widget().tableWidget)))
            rsbe.write.write_file(datalist, filepath)

    def open_project(self):
        pass

    def file_open(self):
        filepath = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', filter='*.csv')[0]
        self.new_spreadsheet(filepath)

    def file_save(self):
        filepath = QtWidgets.QFileDialog.getSaveFileName(self, 'Save File', filter='*.csv')
        DATA = self.get_data(self.mdi.currentSubWindow().widget().tableWidget)
        sp_dataframe = pd.DataFrame(DATA)
        sp_dataframe.to_csv(filepath[0], index_label=False, index=False)

    def new_spreadsheet(self, filepath=None):
        sp = NewSpreadWindow()
        tableWidget = sp.tableWidget

        # Create Sub Windows
        sub = QMdiSubWindow()
        sub.setWindowTitle(str(len(self.mdi.subWindowList())))
        sub.setWidget(sp)
        sub.setMaximumWidth(250)

        # Add the sub window into our mdi widget
        self.mdi.addSubWindow(sub)
        if filepath:
            self.filedata = pd.read_csv(filepath)
            tableWidget.setColumnCount(len(self.filedata.columns))
            tableWidget.setRowCount(len(self.filedata.index))
            for i in range(len(self.filedata.index)):
                for j in range(len(self.filedata.columns)):
                    tableWidget.setItem(i, j, QtWidgets.QTableWidgetItem(str(self.filedata.iat[i, j])))

        self.dataPlott[sub] = ()
        # Show the sub window
        sp.show()


# Fix boundaries
#TODO FIX GEOMETRY PROBLEM
class MdiFixBoundaries(QMdiArea):
    def fixGeometry(self, window, viewGeo):
        winGeo = window.geometry()
        if not viewGeo.contains(winGeo):
            if winGeo.right() > viewGeo.right():
                winGeo.moveRight(viewGeo.right())
            if winGeo.x() < 0:
                winGeo.moveLeft(0)

            if winGeo.bottom() > viewGeo.bottom():
                winGeo.moveBottom(viewGeo.bottom())
            if winGeo.y() < 0:
                winGeo.moveTop(0)
            if winGeo != window.geometry():
                window.setGeometry(winGeo)
                return True
        return False

    def eventFilter(self, obj, event):
        if (event.type() == event.Move and isinstance(obj, QMdiSubWindow) and
                self.fixGeometry(obj, self.viewport().geometry())):
            return True
        return super().eventFilter(obj, event)

    def resizeEvent(self, event):
        super().resizeEvent(event)
        viewGeo = self.viewport().geometry()
        for win in self.subWindowList():
            self.fixGeometry(win, viewGeo)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    ui = UI(r"C:\Users\Lucas\Documents\ic\pyqt-relatorio\Wizards\main.ui")
    app.setStyleSheet(QSS)
    sys.exit(app.exec())