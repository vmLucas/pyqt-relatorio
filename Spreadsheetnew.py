import os
import numpy
import pyqtgraph as pg
from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtWidgets import *
from PySide6.QtUiTools import QUiLoader
from PySide6 import QtUiTools
import numpy as np
import pandas as pd
import time


class NewSpreadWindow(QtWidgets.QWidget):
    def __init__(self, ui):
        super().__init__()
        self.initUI(ui)

    def initUI(self, ui):
        loader = QUiLoader()
        self.ui = NewSpreadWindow.loadUiWidget(self, ui)
        self.plotbutton = self.findChild(QPushButton, 'pushButton')

        self.tableWidget = self.findChild(QTableWidget, "tableWidget")


    def loadUiWidget(self, uifilename):
        loader = QtUiTools.QUiLoader()
        uifile = QtCore.QFile(uifilename)
        uifile.open(QtCore.QFile.ReadOnly)
        ui = loader.load(uifile, self)
        uifile.close()
        return ui

    def mouseLeftClick(self, event: QtGui.QMouseEvent):
        if event.button() == QtCore.Qt.LeftButton:
            print('left click')

    def plot(self):
        data = self.get_data()
        self.resize(1000, 500)
        self.gw.plot(data)
        self.gw.show()

    def get_data(self):
        data = []
        for i in range(self.tableWidget.rowCount()):
            column = []
            for j in range(self.tableWidget.columnCount()):
                column.append(float(self.tableWidget.item(i, j).text()))
            data.append(column)
        return np.array(data)