import os
import numpy
import pyqtgraph as pg
from PySide6 import QtCore, QtGui, QtWidgets
import numpy as np
import pandas as pd
import time


class NewSpreadWindow(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setObjectName("Form")
        self.resize(234, 403)
        self.setGeometry(QtCore.QRect(0, 0, 231, 391))
        self.setObjectName("widget")
        self.gridLayout = QtWidgets.QGridLayout(self)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.tableWidget = QtWidgets.QTableWidget(self)
        self.tableWidget.setRowCount(10)
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setObjectName("tableWidget")
        self.gridLayout.addWidget(self.tableWidget, 0, 0, 1, 1)

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)
        self.gw = pg.PlotWidget()

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("Form", "Form"))




    def get_data(self):
        data = []
        for i in range(self.tableWidget.rowCount()):
            column = []
            for j in range(self.tableWidget.columnCount()):
                column.append(float(self.tableWidget.item(i, j).text()))
            data.append(column)
        return np.array(data)